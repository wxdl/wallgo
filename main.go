package main

import (
	"github.com/PuerkitoBio/goquery"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

var count = 100
var buf = make(chan int, 1)

func ExampleScrape() {

	// Request the HTML page.
	res, err := http.Get("https://wallhaven.cc/search?categories=000&purity=100&atleast=2560x1440&ratios=16x9&sorting=random&order=desc")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	// Find the review items
	doc.Find(".thumb .preview").Each(func(i int, s *goquery.Selection) {
		// For each item found, get the band and title
		src, _ := s.Attr("href")
		download(src)
	})

	index := <-buf
	log.Printf("Finish fetch index:%d", index)

}

func download(url string) {
	// Request the HTML page.
	res, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}
	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	src, _ := doc.Find("#wallpaper").Attr("src")
	log.Printf("src: %s", src)
	res, _ = http.Get(src)
	defer res.Body.Close()
	tokens := strings.Split(src, "/")
	title := tokens[len(tokens)-1]
	op, _ := os.Create(title)
	defer op.Close()
	io.Copy(op, res.Body)
}

func main() {

	for i := 0; i <= 20; i++ {
		buf <- i
		log.Printf("Start fetch %d", i)
		go ExampleScrape()
	}

}
